
## App Ref. Card 01
Standalone Spring Boot Application
 
---
title: App Ref. Card 01
author: Aniisanth Kanagalingam BBW
date: 24.05.2024
---
 
Requirement:
Linux OS
 
 
Link zur Übersicht:<br/>
https://gitlab.com/bbwrl/m347-ref-card-overview
 
 
## Installation der benötigten Werkzeuge
 
Maven Tutorial for Beginners<br/>
https://www.simplilearn.com/tutorials/maven-tutorial
 
 
### Projekt bauen und starten
Die Ausführung der Befehle erfolgt im Projektordner
Builden mit Maven<br/>
 
Refcard 1 is a utility that allows BBW students do magic.
 
 
```$ mvn package```
 
Das Projekt wird gebaut und die entsprechende Jar-Datei im Ordner Target erstellt (Artefakt).
Die erstellte Datei kann nun direkt mit Java gestartet werden.<br/>
```$ java -jar target/app-refcard-01-0.0.1-SNAPSHOT.jar```





